package thor12022.mjolnir;

import thor12022.mjolnir.interfaces.IMod;

public abstract class AbstractMod implements IMod
{
   private String name;
   private String id;
   private String channel;
   private String depend;
   private String version;
   private String clientProxy;
   private String commonProxy;
   
   
   public AbstractMod(Class modInfo)
   {
      try
      {
         name = (String)modInfo.getDeclaredField("NAME").get(modInfo);
         id = (String)modInfo.getDeclaredField("ID").get(modInfo);
         channel = (String)modInfo.getDeclaredField("CHANNEL").get(modInfo);
         depend = (String)modInfo.getDeclaredField("DEPEND").get(modInfo);
         version = (String)modInfo.getDeclaredField("VERSION").get(modInfo);
         clientProxy = (String)modInfo.getDeclaredField("CLIENTPROXY").get(modInfo);
         commonProxy = (String)modInfo.getDeclaredField("COMMONPROXY").get(modInfo);
      }
      catch(Exception excp)
      {
         // If you hit this in a release, I really messed up
         assert(false);
      }
   }
   

   @Override
   public String Name()
   {
      return name;
   }

   @Override
   public String Id()
   {
      return id;
   }

   @Override
   public String Channel()
   {
      return channel;
   }

   @Override
   public String Depend()
   {
      return depend;
   }

   @Override
   public String Version()
   {
      return version;
   }

   @Override
   public String ClientProxy()
   {
      return clientProxy;
   }

   @Override
   public String CommonProxy()
   {
      return commonProxy;
   }

}
