package thor12022.mjolnir.client.gui;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemPiston;
import net.minecraft.item.ItemStack;

import thor12022.mjolnir.interfaces.IMod;

public class CreativeTabBaseMod extends CreativeTabs
{

   public CreativeTabBaseMod(IMod ownerMod, String tabLabel)
   {
      super(tabLabel);
      setBackgroundImageName(ownerMod.Id() + ".png"); // Automagically has
                                                          // tab_ applied to it.
                                                          // Make sure you
                                                          // change the texture
                                                          // name.
   }

   public boolean hasSearchBar()
   {
      return true;
   }

   // The tab icon is what you return here.
   @Override
   public ItemStack getIconItemStack()
   {
      return new ItemStack(new ItemPiston(Blocks.sticky_piston));
   }

   @Override
   public Item getTabIconItem()
   {
      return new Item();
   }
}
