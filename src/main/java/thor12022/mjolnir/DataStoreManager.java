package thor12022.mjolnir;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import com.google.common.io.Files;

import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.nbt.CompressedStreamTools;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.common.DimensionManager;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.event.world.WorldEvent.Load;
import net.minecraftforge.event.world.WorldEvent.Save;
import net.minecraftforge.event.world.WorldEvent.Unload;

import thor12022.mjolnir.interfaces.IMod;
import thor12022.mjolnir.interfaces.INBTStorageClass;

public class DataStoreManager
{
   private final static String SAVE_FOLDER = "thor12022";
   
   private File saveFile = null;   
   private Map<INBTStorageClass, String> storageClasses = new HashMap<INBTStorageClass, String>();
   private IMod ownerMod;
   
   public DataStoreManager(IMod ownerMod)
   {
      MinecraftForge.EVENT_BUS.register(this);
   }
      
   public void addStorageClass( INBTStorageClass theClass, String tagName )
   {
      storageClasses.put(theClass, tagName);
   }
   
   public void updateOldDataFile()
   {
      File worldConfig = DimensionManager.getCurrentSaveRootDirectory();
      File dataSaveFolder = new File( worldConfig.getPath(), ownerMod.Channel() );
      File dataSaveFile = new File(dataSaveFolder, ownerMod.Channel() + ".dat");
      File newDataSaveFolder = new File( worldConfig.getPath(), SAVE_FOLDER );
      File newDataSaveFile = new File(newDataSaveFolder, ownerMod.Channel() + ".dat");
      if(dataSaveFolder.isDirectory() && dataSaveFile.exists())
      {
         try
         {
            Files.move(dataSaveFolder, newDataSaveFile);
         }
         catch(Exception excp)
         {
            ownerMod.Logger().warn("Problem updating old save file: " + dataSaveFile.getAbsolutePath());
         }
      }
   }
   
   private void getDataFile()
   {
      File worldConfig = DimensionManager.getCurrentSaveRootDirectory();
      File dataSaveFolder = new File( worldConfig.getPath(), SAVE_FOLDER );
      if( !dataSaveFolder.isDirectory() && !dataSaveFolder.mkdir() )
      {
         ownerMod.Logger().error("Failed to create " + dataSaveFolder.getAbsolutePath() + " data will not save" );
      }
      else
      {
         saveFile = new File(dataSaveFolder, ownerMod.Channel() + ".dat");
         try
         {
            if( !saveFile.exists() && !saveFile.createNewFile() )
            {
               ownerMod.Logger().error("Failed to create " + saveFile.getAbsolutePath() + " data will not save" );
               saveFile = null;
            }
            else
            {
               ownerMod.Logger().debug("Data file: " + saveFile.getAbsolutePath() + " found/created" );
            }
         } catch (IOException e)
         {
            ownerMod.Logger().error("Failed to create " + saveFile.getAbsolutePath() + " data will not save" );
            saveFile = null;
         }
      }
   }
      
   @SubscribeEvent
   public void onWorldSave(  Save event )
   {
      // we only need to do this once per Save, not per level
      if( event.world.provider.dimensionId != 0 || event.world.isRemote)
      {
         return;
      }
      if( saveFile == null )
      {
         ownerMod.Logger().error("Cannot save data" );
      }
      else
      {
         try
         {
            FileOutputStream fileOutputStream = new FileOutputStream(saveFile);
            NBTTagCompound globalNbt = new NBTTagCompound();
            Iterator iter = storageClasses.keySet().iterator();
            while (iter.hasNext()) 
            {
               NBTTagCompound classNbt = new NBTTagCompound();
               INBTStorageClass theClass = (INBTStorageClass)iter.next();
               theClass.writeToNBT(classNbt);
               globalNbt.setTag(storageClasses.get(theClass), classNbt);
            }
            CompressedStreamTools.writeCompressed(globalNbt, fileOutputStream );
            ownerMod.Logger().debug("Saved data" );
         }
         catch( Throwable e )
         {
            ownerMod.Logger().error("Error saving data: " + e.getLocalizedMessage());
         }
      }
   }
   
   @SubscribeEvent
   public void onWorldUnload(Unload event)
   {
      // we only need to do this once per Save, not per level
      if( event.world.provider.dimensionId != 0 || event.world.isRemote)
      {
         return;
      }
      if( saveFile != null )
      {
         saveFile = null;
      }
   }
   
   @SubscribeEvent
   public void onWorldLoad(Load event)
   {
      // we only need to do this once per Save and only on the server
      if( event.world.provider.dimensionId != 0  || event.world.isRemote)
      {
         return;
      }
      getDataFile();
      if( saveFile == null )
      {
         ownerMod.Logger().error("Cannot load data" );
      }
      else
      {
         FileInputStream fileInputStream = null;
         try
         {
            fileInputStream = new FileInputStream( saveFile );
            NBTTagCompound globalNbt = CompressedStreamTools.readCompressed( fileInputStream );
            Iterator iter = storageClasses.keySet().iterator();
            while (iter.hasNext()) 
            {
               INBTStorageClass theClass = (INBTStorageClass)iter.next();
               theClass.readFromNBT(globalNbt.getCompoundTag(storageClasses.get(theClass)));
            }
            ownerMod.Logger().debug("Data loaded" );
         }
         catch( Throwable e )
         {
            ownerMod.Logger().warn("Failed to  load data " + e.getLocalizedMessage() + ", hopefully a new world.");
         }
      }
   }
}
