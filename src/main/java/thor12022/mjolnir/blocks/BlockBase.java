package thor12022.mjolnir.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;

import thor12022.mjolnir.interfaces.IMod;

public class BlockBase extends Block
{

   // If you aren't setting multiple textures for your block. IE: Non-Metadata
   // blocks.
   public BlockBase(IMod ownerMod, String unlocName, Material material, String textureName, SoundType soundType, float hardness)
   {
      super(material);

      setBlockName(ownerMod.Id() + "." + unlocName);
      setBlockTextureName(ownerMod.Id() + ":" + textureName);
      setCreativeTab(ownerMod.CreativeTabs());
      setStepSound(soundType);
      setHardness(hardness);
   }

   // If you are setting multiple textures for your block. IE: Metadata blocks.
   public BlockBase(IMod ownerMod, String unlocName, Material material, SoundType soundType, float hardness)
   {
      super(material);

      setBlockName(ownerMod.Id() + "." + unlocName);
      setCreativeTab(ownerMod.CreativeTabs());
      setStepSound(soundType);
      setHardness(hardness);
   }
}
