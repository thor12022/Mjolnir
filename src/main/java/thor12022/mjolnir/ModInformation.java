package thor12022.mjolnir;

/*
 * Basic information your mod depends on.
 */

class ModInformation
{

   public static final String NAME = "Mjolnir";
   public static final String ID = "mjolnir";
   public static final String CHANNEL = "Mjolnir";
   public static final String DEPEND = "";
   public static final String VERSION = "0.0.0";
   public static final String CLIENTPROXY = "thor12022.mjolnir.proxies.ClientProxy";
   public static final String COMMONPROXY = "thor12022.mjolnir.proxies.CommonProxy";;
}
