package thor12022.mjolnir.world;

import java.util.HashSet;
import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.entity.Entity;
import net.minecraft.world.ChunkPosition;
import net.minecraft.world.Explosion;
import net.minecraft.world.World;
import net.minecraftforge.common.util.ForgeDirection;

public class ExplosionFractal extends Explosion
{
   protected static final Random RANDOM = new Random();
   
   private float explosionMultiplier = 0.9f;
   private HashSet<ChunkPosition> affectedBlocks = new HashSet();
      
   /**
    * @todo we should just convert base class' worldObj to protected rather than hiding it
    */
   protected World worldObj;
   protected ForgeDirection direction;
   
   public ExplosionFractal(World worldObj, Entity sourceEntity, double xPos, double yPos, double zPos, float size, ForgeDirection direction)
   {
      super(worldObj, sourceEntity, xPos, yPos, zPos, size);
      this.worldObj = worldObj;
      this.direction = direction;
   }
   
   public void doExplosionA()
   {
      for (ForgeDirection dir : ForgeDirection.VALID_DIRECTIONS)
      {
         ChunkPosition nextBlockPos = new ChunkPosition((int)explosionX + dir.offsetX, (int)explosionY + dir.offsetY, (int)explosionZ + dir.offsetZ);
         
         explodeBlock(nextBlockPos, dir.getOpposite(), 0);
      }
      affectedBlockPositions.addAll(affectedBlocks);
      affectedBlocks.clear();
   }
   
   public void explodeBlock( ChunkPosition position, ForgeDirection sourceSide, int depth )
   {
      Block block = worldObj.getBlock(position.chunkPosX, position.chunkPosY, position.chunkPosZ);
      if(block == null)
      {
         return;
      }
      
      float currExplosion = explosionSize - (depth * .125f);
      if(currExplosion < 0f)
      {
         return;
      }
      float f1 = currExplosion * 3f;
      // Air explodes, it's easy
      if (block.getMaterial() == Material.air)
      {
         affectedBlocks.add(position);
      }
      // Other blocks, need some calculation
      else
      {
         float explosionResistance = this.exploder != null ? 
               this.exploder.func_145772_a(this, this.worldObj, position.chunkPosX, position.chunkPosY, position.chunkPosZ, block) 
               : block.getExplosionResistance(this.exploder, worldObj, position.chunkPosX, position.chunkPosY, position.chunkPosZ, explosionX, explosionY, explosionZ);
         f1 -= explosionResistance;
         if(f1 <= 0)
         {
            return;
         }
         if(this.exploder == null || this.exploder.func_145774_a(this, this.worldObj, position.chunkPosX, position.chunkPosY, position.chunkPosZ, block, f1))
         {
             affectedBlocks.add(position);
         }
      }
      
      for (ForgeDirection dir : ForgeDirection.VALID_DIRECTIONS)
      {
         ChunkPosition nextBlockPos = new ChunkPosition(position.chunkPosX + dir.offsetX, position.chunkPosY + dir.offsetY, position.chunkPosZ + dir.offsetZ);
         
         //You can't back to the last position
         if(dir != sourceSide.getOpposite() && !affectedBlocks.contains(nextBlockPos))
         {  
            explodeBlock(nextBlockPos, dir, depth + 1);
         }
     }
   }

}
