package thor12022.mjolnir;

/*
 * Check all the classes for (hopefully) detailed descriptions of what it does. There will also be tidbits of comments throughout the codebase.
 * If you wish to add a description to a class, or extend/change an existing one, submit a PR with your changes.
 */

//import biomesoplenty.api.content.BOPCBiomes;
import java.io.File;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Loader;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStartedEvent;
import cpw.mods.fml.common.event.FMLServerStartingEvent;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.EnumCreatureType;
import net.minecraft.item.crafting.FurnaceRecipes;
import net.minecraft.world.biome.BiomeGenBase;
import net.minecraftforge.common.BiomeDictionary;
import net.minecraftforge.common.BiomeManager;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import thor12022.mjolnir.config.ConfigManager;
import thor12022.mjolnir.entity.EntityRegistry;
import thor12022.mjolnir.interfaces.IMod;
import thor12022.mjolnir.proxies.CommonProxy;
import thor12022.mjolnir.util.TextHelper;

@Mod(modid = ModInformation.ID, name = ModInformation.NAME, version = ModInformation.VERSION, dependencies = ModInformation.DEPEND)
public class Mjolnir extends AbstractMod
{
   @SidedProxy(clientSide = ModInformation.CLIENTPROXY, serverSide = ModInformation.COMMONPROXY)
   public static CommonProxy proxy;

   private Logger logger = LogManager.getLogger(Name());
   private ConfigManager configManager = new ConfigManager(this);
   
   
   Mjolnir()
   {
      super(ModInformation.class);
   }


   @Mod.Instance
   public static Mjolnir instance;

   @Mod.EventHandler
   public void preInit(FMLPreInitializationEvent event)
   {
      logger.info(TextHelper.localize("info." + Id() + ".console.load.preInit"));

      configManager.init(event.getModConfigurationDirectory());
   }

   @Override
   public CreativeTabs CreativeTabs()
   {
      return null;
   }

   @Override
   public Logger Logger()
   {
      return logger;
   }
}
