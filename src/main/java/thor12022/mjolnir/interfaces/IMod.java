package thor12022.mjolnir.interfaces;

public interface IMod
{
   String Name();
   
   String Id();
   
   String Channel();
   
   String Depend();
   
   String Version();
   
   String ClientProxy();
   
   String CommonProxy();
   
   net.minecraft.creativetab.CreativeTabs CreativeTabs();
   
   org.apache.logging.log4j.Logger Logger();
}
