package thor12022.mjolnir.items;

import net.minecraft.item.Item;

import thor12022.mjolnir.interfaces.IMod;

public class ItemBase extends Item
{

   // If you aren't setting multiple textures for your item. IE: Non-Metadata
   // items.
   public ItemBase(IMod ownerMod, String unlocName, String textureName)
   {
      super();

      setUnlocalizedName(ownerMod.Id() + "." + unlocName);
      setTextureName(ownerMod.Id() + ":" + textureName);
      setCreativeTab(ownerMod.CreativeTabs());
   }

   // If you are setting multiple textures for your item. IE: Metadata items.
   public ItemBase(IMod ownerMod, String unlocName)
   {
      super();

      setUnlocalizedName(ownerMod.Id() + "." + unlocName);
      setCreativeTab(ownerMod.CreativeTabs());
   }
}
