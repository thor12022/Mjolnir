package thor12022.mjolnir.config;

/*
 * Creation and usage of the config file.
 */

import net.minecraftforge.common.config.Configuration;

import thor12022.mjolnir.Mjolnir;
import thor12022.mjolnir.interfaces.IMod;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class ConfigManager
{
   /**
    * @todo This was supposed to map IConfigClasses to Categories in a single Configuration
    *    however, this proved impractical as the Configuration class and ConfigurationCategory
    *    class do not provide high level methods for ConfigurationCategory. It doesn't have 
    *    getString getInt getDouble, etc . It should function similarly to the DataStoreManager,
    *    passing to each IConfigClass an object containing only its own data
    */
   private Map<IConfigClass, Configuration> configClasses = new HashMap<IConfigClass, Configuration>();
   private File configDirectory;
   private IMod ownerMod;
   
   public void init(File configDirectory)
   {
      this.configDirectory = new File(configDirectory.getAbsolutePath() + File.separator + ownerMod.Channel());
      syncConfig();
   }
   
   public ConfigManager(IMod ownerMod)
   {
      this.ownerMod = ownerMod;
   }
   
   public void addConfigClass( IConfigClass theClass )
   {
      Configuration theConfig = new Configuration(new File( configDirectory  + File.separator + theClass.getSectionName() + ".cfg"));
      configClasses.put(theClass, theConfig);
      theClass.syncConfig(theConfig);
      theConfig.save();
   }

   public void syncConfig()
   {
      try
      {
         Iterator iter = configClasses.keySet().iterator();
         while (iter.hasNext()) 
         {
            IConfigClass theClass = (IConfigClass)iter.next();
            Configuration theConfig = configClasses.get(theClass);
            theClass.syncConfig(theConfig);
            theConfig.save();
         }
         ownerMod.Logger().debug("Synced config data");
      }
      catch( Throwable e )
      {
         ownerMod.Logger().error("Error syncing config data: " + e.getLocalizedMessage());
      }
   }
}
