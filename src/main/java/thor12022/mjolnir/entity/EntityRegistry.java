package thor12022.mjolnir.entity;

import cpw.mods.fml.common.Mod;
import net.minecraft.entity.Entity;
import thor12022.mjolnir.interfaces.IMod;

public class EntityRegistry
{
   private int   entityId = 0;
   private Mod   ownerMod;
	
   public EntityRegistry( Mod mod )
   {
      ownerMod = mod;
   }
   
   public int registerEntity(Class<? extends Entity> entityClass, String entityUnlocalizedName, int trackingRange, int updateFrequency, boolean sendsVelocityUpdates)
    {
       cpw.mods.fml.common.registry.EntityRegistry.registerModEntity(
             entityClass,
             entityUnlocalizedName,
             entityId++, 
             ownerMod, 
             trackingRange, 
             updateFrequency, 
             sendsVelocityUpdates);
       return entityId;
   }
}
