This is the Common Support Library for Thor12022's Minecraft mods

Requires a minimum of Forge 1384.

Feel free to use in modpacks; notification is appreciated, but not required.
